# AntiPhishing - Help  

[Back to All Support](https://github.com/mrTownsend/BeeHive-Support)

**AntiPhishing** is a fantastic tool to help protect your server from spambots and hacked accounts sending malicious links in your channels. By **DEFAULT**, BeeHive **WILL** delete confirmed malicious links silently. If you need to configure it, there is one command and it is very simple.

# Screenshots
>Help Menu

![Help Menu](https://i.imgur.com/72XbVrj.png)

> Malicious Link Detected Message

![Detection Message](https://i.imgur.com/2zqKBza.png)

> Default Settings

![Default Settings](https://i.imgur.com/Vi56iCt.png)

# Commands

>[p]antiphishing settings
>>Shows current AntiPhishing settings. AntiPhishing is always enabled by default.

>[p]antiphishing log <#channel>
>>Lets you set a logging channel for phishing reports

>[p]antiphishing publicalerts <#channel>
>> Lets you specify a channel so regular server members can be alerted to malicious users in the server. Helps bring awareness to users who are DMing malicious links and things of that sort. I recommend setting this to your server's "general" chat channel.
