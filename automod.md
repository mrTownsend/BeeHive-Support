# AutoMod - Help  

[Back to All Support](https://github.com/mrTownsend/BeeHive-Support)

**AutoMod** is one of the most important tools when it comes to protecting your server! AutoMod includes tools to guard against spam, mentions, raiding, and other types of behavior. Note that this is not the **AI** portion of BeeHive, this is a basic but dedicated AutoMod.

# Screenshots + Demo

Settings Embed:

![Settings Embed](https://i.imgur.com/sQXN8h6.png)

Example of Configuring an Action for a Specific Violation:

![General Spam Action Config](https://i.imgur.com/AOcs3Po.png)


# Commands

>[p]allowedextensionsrule
>>Blacklist or whitelist extensions

>[p]automodset
>>Change AutoMod settings.

>[p]inviterule
>>Filters discord invites

>[p]maxcharsrule
>>Detects the maximum allowed individual characters per message

>[p]maxwordsrule
>>Detects the maximum allowed length of individual words per message

>[p]mentionspamrule
>>Individual mentions spam settings

>[p]spamrule
>>Mass spamming by user or content

>[p]wallspamrule
>>Walls of text/emojis settings

>[p]wordfilterrule
>>Detects if a word matches list of forbidden words.
