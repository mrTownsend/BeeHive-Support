# Partnerships, Massing, and Bumping

[Return to Support](http://github.com/mrTownsend/BeeHive-Support)

BeeHive is one of the few bots proud to be partner (and partner manager) friendly.

If you are a Discord "Partner Manager", please get in touch with us to join our verified PM team and gain access to BeeHive's various partnership/massing commands.

If you are a server owner, here's how this works.

> 1. You reach out to one of our Verified Partner Managers, and work out a free or paid deal for them to "mass" your server.

> 2. You set a channel in your server that is publicly viewable (your PM will guide you on which) using ~setmasschannel

> 3. Your PM will mass your server according to schedule, and you'll see other server's masses show up in your set partnership channel. It's that easy!
