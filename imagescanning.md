# Image Scanning - Help

[Return to Support](http://github.com/mrTownsend/BeeHive-Support)


One of the things that you'll find helpful about BeeHive is that you can privately analyze images for content that breaks your server's rules and automatically delete it if so. This module interfaces individually with the SightEngine API, so included below will be some install instructions to prevent you from getting overwhelmed.

# Screenshots

Screenshots are coming soon!

# Commands

>[p]imagescanning credentials
>>Set the API Credentials to use with Image Scanning.

>[p]imagescanning detect
>>Group command for changing what is detected by image scanning.

>[p]imagescanning lists
>>Manage per-channel whitelists and blacklists.

>[p]imagescanning report
>>Manage how reports are handled by the bot.

>[p]imagescanning settings
>>View registered settings

# Setup Instructions

Setup instructions are coming soon!
