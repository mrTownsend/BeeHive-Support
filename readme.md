# BeeHive Support Repo

![Discord Bots](https://top.gg/api/widget/878370684542263337.svg)
> **Vote for BeeHive!**
>>[Vote on Top.GG](https://top.gg/bot/878370684542263337)

**Hi!** If you're reading this I assume you're a user, so I'd like to say thank you for supporting BeeHive! Now, we've got this repository divided up into individual files for each module, so you can
only have to read about the commands and configuration information you're interested in.

If you're actively having any issues with BeeHive, or any of its' modules, please [open a GitHub Issue](https://github.com/mrTownsend/BeeHive-Support/issues/new/choose). This is the easiest way for me to be able to troubleshoot errors. In the issue, please include the time, day, server ID, message ID or if the error was in DMs, along with any other information you can provide me. A little goes a long way when it comes to development.

Lastly, if you need to speak to a human or have a question you believe would be better answered by interacting with a human, please join our [Support Discord](https://discord.gg/gnhEe28CuW). You'll be greeted on entry with a CAPTCHA powered by BeeHive, and you'll be able to talk to us once you pass the CAPTCHA. If you do not respond to the CAPTCHA, or refuse to answer the CAPTCHA for some dumbass reason, you'll be kicked. **ALSO** if you're reading this and think that server is a good idea to try to raid - please try. You're in for a treat.

---
# Help Index
Click the link for the name of the module or feature you'd like to see help information about.

>[Help For CAPTCHA](https://github.com/mrTownsend/BeeHive-Support/blob/main/captcha.md)

>[Help for AntiPhishing](https://github.com/mrTownsend/BeeHive-Support/blob/main/antiphishing.md)

>[Help for AutoMod](https://github.com/mrTownsend/BeeHive-Support/blob/main/automod.md)

>[Help for Email Verification](https://github.com/mrTownsend/BeeHive-Support/blob/main/emailverification.md)

>[Help for Music](https://github.com/mrTownsend/BeeHive-Support/blob/main/music.md)

>[Help for Bot Statistics](https://github.com/mrTownsend/BeeHive-Support/blob/main/botstatistics.md)

>[Help for Image Scanning](https://github.com/mrTownsend/BeeHive-Support/blob/main/imagescanning.md)

>[Help for Partnerships and Partner Managers](https://github.com/mrTownsend/BeeHive-Support/blob/main/partnerships.md)
